# Sensors porting

This repository contains logs from downstream Android devices that are used to
figure out the protocols and format of the sensors.

# Links
- [Qualcomm-Sensorcomm-Docs.md](https://codeberg.org/DylanVanAssche/libssc/src/branch/main/docs/Qualcomm-Sensorcore-Docs.md)
